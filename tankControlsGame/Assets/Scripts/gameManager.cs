﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameManager : MonoBehaviour
{
    //game manager instance
    public static gameManager game;

    //player variables
    public float playerTurnSpeed = 150.0f;
    public float playerMoveSpeed = 10.0f;
    public GameObject bulletSpawn; //spawn point for the bullet, a little further ahead of player
    public GameObject bulletDoubleOne;
    public GameObject bulletDoubleTwo;
    public int playerLives = 3;
    public GameObject player = null;

    //bullet variables
    public GameObject bulletPrefab = null;
    public float bulletTime = 0.5f;

    //enemy variables
    public List<GameObject> enemyList;
    public List<GameObject> liveEnemies;
    public Transform[] enemySpawnPoints;
    public int shipPoints = 150;
    public int asteroidPoints = 50;
    private int maxEnemies = 3;
    private float enemySpawnRate = 3.0f;
    private float enemyNextSpawn = 0.0f;

    //powerups
    public GameObject doubleShotPre = null;
    public enum PowerUp
    {
        Normal, //0
        DoubleShot, //1
        DoubleSpeed //2
    }
    public PowerUp powerUp;
    public bool doubleShotOn = false;
    public Transform[] powerUpSpawns;
    public float powerUpRate = 10.0f;
    public float powerUpNext = 5.0f;
    public List<GameObject> activePowerUps;
    private int maxPowerUps = 2;

    //UI
    public Text livesText;
    public Text scoreCount;
    public Text doubleShotCount;
    public int pointsEarned = 0;
    public int shotsLeft = 0;

    void Awake()
    {
        if (game == null)
        {
            game = this; //storing this instance of the class in game
        }
        else
        {
            Destroy(this.gameObject); //if a new object is made, destroy it
        }
    }

    // Use this for initialization
    void Start ()
    {
        liveEnemies = new List<GameObject>();
        SetLivesText();
        UpdateScore();
        UpdateDoubleShots();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (liveEnemies.Count < maxEnemies && Time.time > enemyNextSpawn) //spawn enemies if less than 3
        {
            enemyNextSpawn = Time.time + enemySpawnRate; //spawn time

            int id = Random.Range(0, enemySpawnPoints.Length); //spawn point 
            GameObject newEnemy = enemyList[Random.Range(0, enemyList.Count)]; //type of enemy 

            GameObject enemyInstance = Instantiate<GameObject>(newEnemy, enemySpawnPoints[id].transform.position, Quaternion.identity); //instantiate the enemy 
            liveEnemies.Add(enemyInstance);
        }

        if (activePowerUps.Count < maxPowerUps && Time.time > powerUpNext)
        {
            powerUpNext = Time.time + powerUpRate;

            int id = Random.Range(0, powerUpSpawns.Length);
            GameObject powerInstance = Instantiate<GameObject>(doubleShotPre, powerUpSpawns[id].transform.position, Quaternion.identity);
            activePowerUps.Add(powerInstance);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (shotsLeft == 0)
        {
            SetPowerUp(0);
        }

	}

    void ActivatePowerUps()
    {
        switch (powerUp)
        {
            case PowerUp.Normal:
                bulletSpawn.SetActive(true);
                bulletDoubleOne.SetActive(false);
                bulletDoubleTwo.SetActive(false);
                doubleShotOn = false;
                playerMoveSpeed = 10.0f;
                break;

            case PowerUp.DoubleShot:
                bulletSpawn.SetActive(false);
                bulletDoubleOne.SetActive(true);
                bulletDoubleTwo.SetActive(true);
                doubleShotOn = true;
                playerMoveSpeed = 10.0f;
                shotsLeft += 20;
                UpdateDoubleShots();
                break;

            case PowerUp.DoubleSpeed:
                bulletSpawn.SetActive(true);
                bulletDoubleOne.SetActive(false);
                bulletDoubleTwo.SetActive(false);
                doubleShotOn = false;
                playerMoveSpeed = 20.0f;
                break;
        }
    }

    public void SetPowerUp(int powerup)
    {
        powerUp = (PowerUp)powerup;
        ActivatePowerUps();
    }
    
    public void destroyAllEnemies()
    {
        for(int i = 0; i < liveEnemies.Count; i++)
        {
            Destroy(liveEnemies[i]);
        }
        liveEnemies.Clear();
    }

    public void SetLivesText()
    {
        livesText.text = "Lives Remaining: " + playerLives.ToString();
    }

    public void UpdateScore()
    {
        scoreCount.text = "Score: " + pointsEarned.ToString();
    }

    public void UpdateDoubleShots()
    {
        doubleShotCount.text = "Power Up: " + shotsLeft.ToString();
    }

}
