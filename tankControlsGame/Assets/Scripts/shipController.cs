﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shipController : MonoBehaviour
{
    private Transform tf; //transform object
    public float nextFire = 0.0f; //when the player  can fire next 
    public float fireRate = 0.5f; //fire rate 

    // Use this for initialization
    void Start ()
    {
        tf = GetComponent<Transform>(); //getting the transform
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) //rotate left controls
        {
            tf.Rotate(0, 0, (gameManager.game.playerTurnSpeed * Time.deltaTime));
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) //rotate right controls 
        {
            tf.Rotate(0, 0, (-gameManager.game.playerTurnSpeed * Time.deltaTime));
        }

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) //move the ship up from the direction it is facing 
        {
            tf.Translate(Vector3.up * gameManager.game.playerMoveSpeed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) //move the ship down from the direction it is facing
        {
            tf.Translate(Vector3.down * gameManager.game.playerMoveSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.Space) && Time.time > nextFire) //fire bullet
        {
            nextFire = Time.time + fireRate; //update next fire
            if (gameManager.game.doubleShotOn == true)
            {
                GameObject bulletOne = Instantiate<GameObject>(gameManager.game.bulletPrefab, gameManager.game.bulletDoubleOne.transform.position, gameManager.game.bulletDoubleOne.transform.rotation);
                GameObject bulletTwo = Instantiate<GameObject>(gameManager.game.bulletPrefab, gameManager.game.bulletDoubleTwo.transform.position, gameManager.game.bulletDoubleTwo.transform.rotation);
                gameManager.game.shotsLeft--;
                gameManager.game.UpdateDoubleShots();
            }
            else
            {
                GameObject bullet = Instantiate<GameObject>(gameManager.game.bulletPrefab, gameManager.game.bulletSpawn.transform.position, gameManager.game.bulletSpawn.transform.rotation);
            }
        }
	}

    void LoseLife() //losing lives 
    {
        gameManager.game.playerLives--; //decrement lives 
        if (gameManager.game.playerLives == 0) //if 0 quit game 
        {
            Application.Quit();
        }
        else //reset to origin
        {
            tf.position = Random.insideUnitCircle;
        }
    }


    void OnTriggerExit2D(Collider2D other) //if player leaves game
    {
        LoseLife();
        gameManager.game.SetLivesText();
    }

    void OnCollisionEnter2D(Collision2D other) //if player collides with enemies
    {
        LoseLife();
        gameManager.game.SetLivesText();
        gameManager.game.destroyAllEnemies(); //destroy all enemies if player hits one
    }
}
