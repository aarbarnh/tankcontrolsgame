﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidController : MonoBehaviour
{
    private Rigidbody2D rb;
    public float moveSpeed = 4.0f; //public movement speed 

	// Use this for initialization
	void Start ()
    {

	}

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>(); //get rigidbody 2d

        Vector3 direction = gameManager.game.player.transform.position - transform.position; //get the direction to the player
        rb.AddForce(direction * moveSpeed); //move towards the player
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    void OnCollisionEnter2D(Collision2D other) //if the asteroid collides with the bullet, destroy both
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            gameManager.game.pointsEarned += gameManager.game.asteroidPoints; //add to points earned 
            gameManager.game.UpdateScore(); //update score 
            gameManager.game.liveEnemies.Remove(this.gameObject);
            Destroy(this.gameObject);
            Destroy(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other) //if the asteroid leaves the level it will be destroyed
    {
        gameManager.game.liveEnemies.Remove(this.gameObject);
        Destroy(this.gameObject);
    }
}
