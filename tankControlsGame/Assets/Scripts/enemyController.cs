﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyController : MonoBehaviour
{
    public float moveSpeed = 2.0f; //move speed public
    public float rotationSpeed = 125.0f; //rotation speed public 
    public Vector3 direction;

    // Use this for initialization
    void Start ()
    {
        direction = new Vector3(1, 0, 0); //initialize direction 
    }
	
	// Update is called once per frame
	void Update () 
    {
        direction = new Vector3(1, 0, 0);
        transform.Translate(direction * Time.deltaTime * moveSpeed); //give initial movement 

        //gradual targeting
        direction = gameManager.game.player.transform.position - transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(0, 0, angle);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        transform.Translate(Time.deltaTime * moveSpeed, 0, 0);
    }

    void OnCollisionEnter2D(Collision2D other) //if the asteroid collides with the bullet, destroy both
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            gameManager.game.pointsEarned += gameManager.game.shipPoints; //add to points earned 
            gameManager.game.UpdateScore(); //update score 
            gameManager.game.liveEnemies.Remove(this.gameObject);
            Destroy(this.gameObject);
            Destroy(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other) //if the asteroid leaves the level it will be destroyed
    {
        gameManager.game.liveEnemies.Remove(this.gameObject);
        Destroy(this.gameObject);
    }
}
