﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    private Transform tf;
    public float bulletSpeed = 30.0f; //bullet speed, public

	// Use this for initialization
	void Start ()
    {
        tf = GetComponent<Transform>();
        Destroy(this.gameObject, gameManager.game.bulletTime);
	}
	
	// Update is called once per frame
	void Update ()
    {
        tf.Translate(Vector3.up * bulletSpeed * Time.deltaTime); //make the bullet go up at movement speed 
    }

}
