﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioClip bulletSound;
    public AudioSource bulletSource;
    public float nextSound = 0.0f;
    public float bulletSoundRate = 0.5f;

	// Use this for initialization
	void Start ()
    {
        bulletSource.clip = bulletSound;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKey(KeyCode.Space) && Time.time > nextSound)
        {
            nextSound = Time.time + bulletSoundRate;
            bulletSource.Play();
        }
	}
}
