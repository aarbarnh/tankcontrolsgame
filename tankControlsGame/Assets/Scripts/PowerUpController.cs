﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{
    private Transform tf;

	// Use this for initialization
	void Start ()
    {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            OnDoubleShotPickUp();
            gameManager.game.activePowerUps.Remove(this.gameObject);
            Destroy(this.gameObject);
        }
    }

    void OnDoubleShotPickUp()
    {
        gameManager.game.SetPowerUp(1);
    }
}
